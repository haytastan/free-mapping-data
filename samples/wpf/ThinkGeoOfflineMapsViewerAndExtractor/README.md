# ThinkGeo Maps Street Viewer & Extractor

### Description

This sample shows how to display the ThinkGeo Maps Street SQLite vector tile data in a WPF desktop application. The data used in this sample is a 16.3M mbtiles subset covering New York City cut from the full ThinkGeo Map Streets SQLite dataset available for purchase.  

In addition this sample also shows how to extract and create smaller MBTile datsets for a given extent. This can be helfpull if you don't want to deploy the entire ThinkGeo Maps Street  SQLite database with your application as it can be quite large.

This sample uses .NET Core 3.1, which can be easily upgraded to .NET 5 or .NET 6 by changing its project properties. The exact same features also work with .NET Framework (4.6.2 and above) if you create a new .NET Framework project and copy all the source files over. 

*.MBTile format is supported in both ThinkGeo Desktop, ThinkGeo Mobile and ThinkGeo Web. 

![Screenshot](./Screenshot.gif)

### About the Code
```csharp
string mbtilesPathFilename = "Data/NewYorkCity.mbtiles";
string defaultJsonFilePath = "Data/thinkgeo-world-streets-light.json";

this.wpfMap.MapUnit = GeographyUnit.Meter;
ThinkGeoMBTilesLayer thinkGeoMBTilesFeatureLayer = new ThinkGeoMBTilesLayer(mbtilesPathFilename, new Uri(defaultJsonFilePath, UriKind.Relative));
            
layerOverlay = new LayerOverlay();
layerOverlay.Layers.Add(thinkGeoMBTilesFeatureLayer);

this.wpfMap.Overlays.Add(layerOverlay);
```
### Getting Help

[Map Suite UI Control for WPF Wiki Resources](https://wiki.thinkgeo.com/wiki/thinkgeo_desktop_for_wpf)

[Map Suite UI Control for WPF Product Description](https://thinkgeo.com/gis-ui-desktop#platforms)

[ThinkGeo Community Site](http://community.thinkgeo.com/)

[ThinkGeo Web Site](http://www.thinkgeo.com)

### About ThinkGeo
ThinkGeo is a GIS (Geographic Information Systems) company founded in 2004 and located in Frisco, TX. Our clients are in more than 40 industries including agriculture, energy, transportation, government, engineering, software development, and defense.
