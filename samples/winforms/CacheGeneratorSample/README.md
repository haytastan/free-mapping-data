# Cache Generator Sample for WinForms

### Description

This utility can help us to generate tile cache.

![Screenshot](https://gitlab.com/thinkgeo/public/mapping-data/-/raw/master/samples/winforms/CacheGeneratorSample/Screenshot.png)


### Requirements
This sample makes use of the following NuGet Packages

[MapSuite 12.0.0.0](https://www.nuget.org/packages?q=thinkgeo)


### Getting Help

[ThinkGeo Desktop Maps Quickstart](https://docs.thinkgeo.com/products/desktop-maps/v12.0/quickstart/)

[ThinkGeo Community Site](http://community.thinkgeo.com/)

[ThinkGeo Web Site](http://www.thinkgeo.com)

### About ThinkGeo
ThinkGeo is a GIS (Geographic Information Systems) company founded in 2004 and located in Frisco, TX. Our clients are in more than 40 industries including agriculture, energy, transportation, government, engineering, software development, and defense.
