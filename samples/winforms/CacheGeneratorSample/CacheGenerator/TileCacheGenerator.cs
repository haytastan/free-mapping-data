﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using ThinkGeo.Core;

namespace CacheGenerator
{
    class TileCacheGenerator
    {
        private int tileWidth = 256;
        private int tileHeight = 256;
        // We have this maxTileCountInOneBitmap for optimization. we would rather draw a 4096*4096 bitmap once and split it to 256 tiles, 
        // than draw a 256*256 tile for 256 times. 
        private int maxTileCountInOneBitmap = 16;

        private int threadsCount;
        private short jpegQuality;
        private RasterTileFormat tileImageFormat;
        Collection<Layer> layersToCache;
        GeographyUnit mapUnit;
        Collection<double> scales;
        RectangleShape cachingExtent;
        string cacheFolder;
        string restrictShapeFilePathName;
        Bitmap watermarkBitmap;
        ShapeFileFeatureLayer restrictionLayer;

        int currentTileIndex = 0;
        long totalTilesCount = 0;
        Collection<Thread> workingThreads;
        int workingThreadCount;

        public event EventHandler<CacheGeneratorProgressChangedEventArgs> ProgressChanged;
        public event EventHandler<EventArgs> GenerationCompleted;

        Collection<CreatingCellsArgument> cellsArguments = new Collection<CreatingCellsArgument>();

        public TileCacheGenerator()
        {
            ThreadsCount = 4;
        }

        public int ThreadsCount
        {
            get { return threadsCount; }
            set { threadsCount = value; }
        }

        public int TileWidth
        {
            get { return tileWidth; }
            set { tileWidth = value; }
        }

        public int TileHeight
        {
            get { return tileHeight; }
            set { tileHeight = value; }
        }

        public RasterTileFormat TileImageFormat
        {
            get { return tileImageFormat; }
            set { tileImageFormat = value; }
        }

        public short JpegQuality
        {
            get { return jpegQuality; }
            set { jpegQuality = value; }
        }

        public Collection<Layer> LayersToCache
        {
            get { return layersToCache; }
            set { layersToCache = value; }
        }

        public GeographyUnit MapUnit
        {
            get { return mapUnit; }
            set { mapUnit = value; }
        }

        public Collection<double> ScalesToCache
        {
            get { return scales; }
            set { scales = value; }
        }

        public RectangleShape CachingExtent
        {
            get { return cachingExtent; }
            set { cachingExtent = value; }
        }

        public string CacheFolder
        {
            get { return cacheFolder; }
            set { cacheFolder = value; }
        }

        public string RestrictShapeFilePathName
        {
            get { return restrictShapeFilePathName; }
            set { restrictShapeFilePathName = value; }
        }

        public Bitmap WatermarkBitmap
        {
            get { return watermarkBitmap; }
            set { watermarkBitmap = value; }
        }

        public void GenerateTiles()
        {
            if (!string.IsNullOrEmpty(restrictShapeFilePathName))
            {
                restrictionLayer = new ShapeFileFeatureLayer(restrictShapeFilePathName, FileAccess.Read);
                restrictionLayer.Open();
            }

            currentTileIndex = 0;
            totalTilesCount = GetTilesCount();

            workingThreads = new Collection<Thread>();
            Thread thread = new Thread(new ParameterizedThreadStart(GenerateTiles));
            thread.Start(scales);
            workingThreads.Add(thread);
        }

        private void GenerateTiles(object scales)
        {
            workingThreadCount = this.ThreadsCount;

            Collection<double> doubleScales = (Collection<double>)scales;
            foreach (double scale in doubleScales)
            {
                GenerateTilesForOneScale(scale);
            }

            ManualResetEvent manualResetEvent = new ManualResetEvent(false);
            for (int i = 0; i < this.ThreadsCount; i++)
            {
                Thread thread = new Thread(new ParameterizedThreadStart(CreateTiles));
                thread.Start(manualResetEvent);
                workingThreads.Add(thread);
            }

            manualResetEvent.WaitOne();

            foreach (Layer layer in layersToCache)
            {
                if (layer.IsOpen)
                {
                    layer.Close();
                }
            }

            if (restrictionLayer != null && restrictionLayer.IsOpen)
            {
                restrictionLayer.Close();
            }

            OnGenerationCompleted(new EventArgs());
        }

        private void GenerateTilesForOneScale(object scale)
        {
            RectangleShape boundingbox = GetBoundingboxFromMapUnit();

            TileMatrix tileMatrix = new TileMatrix((double)scale, tileHeight, tileWidth, boundingbox, mapUnit);

            FileRasterTileCache tileCache = new FileRasterTileCache(cacheFolder, string.Empty, TileImageFormat);
            tileCache.JpegQuality = JpegQuality;

            RectangleShape matrixBoundingBox = tileMatrix.BoundingBox;

            RowColumnRange rowColumnRange = tileMatrix.GetIntersectingRowColumnRange(matrixBoundingBox);

            int deltaColumnIndex = (int)(rowColumnRange.MaxColumnIndex - rowColumnRange.MinColumnIndex);
            int deltaRowIndex = (int)(rowColumnRange.MaxRowIndex - rowColumnRange.MinRowIndex);
            int deltaIndex = Math.Max(deltaColumnIndex, deltaRowIndex);

            double cellWidth = tileMatrix.CellWidth;
            double cellHeight = tileMatrix.CellHeight;

            if (deltaIndex < maxTileCountInOneBitmap)
            {
                int bitMapWidth = tileWidth * (deltaColumnIndex + 1);
                int bitMapHeight = tileHeight * (deltaRowIndex + 1);

                CreateTiles(tileCache, tileMatrix, matrixBoundingBox, bitMapWidth, bitMapHeight, watermarkBitmap, restrictionLayer);
            }
            else
            {

                CreatingCellsArgument cellArgument = new CreatingCellsArgument();

                cellArgument.CellWidth = cellWidth * maxTileCountInOneBitmap;
                cellArgument.CellHeight = cellHeight * maxTileCountInOneBitmap;

                int startColumnIndex = (int)((cachingExtent.UpperLeftPoint.X - matrixBoundingBox.UpperLeftPoint.X) / cellArgument.CellWidth);
                int startRowIndex = (int)((matrixBoundingBox.UpperLeftPoint.Y - cachingExtent.UpperLeftPoint.Y) / cellArgument.CellHeight);

                cellArgument.StartRowIndex = startRowIndex;
                cellArgument.StartColumnIndex = startColumnIndex;

                int endColumnIndex = (int)Math.Ceiling((cachingExtent.UpperRightPoint.X - matrixBoundingBox.UpperLeftPoint.X) / cellArgument.CellWidth);
                int endRowIndex = (int)Math.Ceiling((matrixBoundingBox.UpperLeftPoint.Y - cachingExtent.LowerLeftPoint.Y) / cellArgument.CellHeight);

                cellArgument.ColumnCount = endColumnIndex - startColumnIndex;
                cellArgument.RowCount = endRowIndex - startRowIndex;

                cellArgument.TileMatrix = tileMatrix;
                cellArgument.TileCache = tileCache;
                cellsArguments.Add(cellArgument);
            }
        }

        private RectangleShape GetBoundingboxFromMapUnit()
        {
            RectangleShape boundingbox;
            if (mapUnit == GeographyUnit.DecimalDegree)
            {
                boundingbox = MaxExtents.DecimalDegree;
            }
            else if (mapUnit == GeographyUnit.Meter)
            {
                boundingbox = MaxExtents.SphericalMercator;
            }
            else if (mapUnit == GeographyUnit.Feet)
            {
                boundingbox = MaxExtents.Feet;
            }
            else
            {
                throw new Exception("unknown map unit");
            }

            return boundingbox;
        }

        private void CreateTiles(object manualResetEvent)
        {
            ManualResetEvent resetEvent = (ManualResetEvent)manualResetEvent;

            Bitmap clonedWatermarkBitmap = GetWatermarkBitmap();
            ShapeFileFeatureLayer clonedRestrictedLayer = GetRestrictionLayer();

            while (true)
            {
                CreatingTilesArgument currentArgument = GetTilesCreatingArgument();
                if (currentArgument != null)
                {
                    CreateTiles(currentArgument.TileCache, currentArgument.TileMatrix, currentArgument.Extent, currentArgument.BitmapWidth, currentArgument.BitmapHeight, clonedWatermarkBitmap, clonedRestrictedLayer);
                }
                if (cellsArguments.Count == 0)
                {
                    break;
                }
            }
            if (clonedRestrictedLayer != null)
            {
                clonedRestrictedLayer.Close();
            }

            if (Interlocked.Decrement(ref workingThreadCount) == 0)
            {
                resetEvent.Set();
            }
        }

        private CreatingTilesArgument GetTilesCreatingArgument()
        {
            CreatingTilesArgument tilesCreatingArgument = null;
            lock (cellsArguments)
            {
                if (cellsArguments.Count == 0)
                {
                    return null;
                }

                CreatingCellsArgument tileArgument = cellsArguments[0];

                if (tileArgument.CurrentRowIndex == tileArgument.RowCount && tileArgument.CurrentColumnIndex == tileArgument.ColumnCount)
                {
                    cellsArguments.RemoveAt(0);
                    tilesCreatingArgument = GetTilesCreatingArgument();
                }
                else
                {
                    RectangleShape totalExtent = tileArgument.GetCurrentCellExtent();

                    // This is a workaround of an issue or RectangleShape. 
                    MultipolygonShape intersection = ((AreaBaseShape)cachingExtent).GetIntersection(totalExtent);

                    if (intersection != null)
                    {
                        tilesCreatingArgument = new CreatingTilesArgument(tileArgument.TileCache, tileArgument.TileMatrix, totalExtent, maxTileCountInOneBitmap * tileWidth, maxTileCountInOneBitmap * tileHeight);
                    }
                    tileArgument.CurrentColumnIndex++;
                }


                if (tileArgument.CurrentColumnIndex == tileArgument.ColumnCount
                    && tileArgument.CurrentRowIndex != tileArgument.RowCount)
                {
                    tileArgument.CurrentRowIndex++;
                    tileArgument.CurrentColumnIndex = 0;
                }
            }
            return tilesCreatingArgument;
        }

        private void CreateTiles(FileRasterTileCache tileCache, TileMatrix tileMatrix, RectangleShape extent, int bitmapWidth, int bitmapHeight, Bitmap watermarkBitmap, ShapeFileFeatureLayer restrictionLayer)
        {
            if (restrictionLayer == null || restrictionLayer.QueryTools.GetFeaturesIntersecting(extent, ReturningColumnsType.NoColumns).Count > 0)
            {
                GeoImage geoImage = new GeoImage(bitmapWidth, bitmapHeight);
                DrawOnBitmap(LayersToCache, mapUnit, extent, geoImage);
                SaveBitmapToTiles(tileCache, tileMatrix, geoImage, extent, cachingExtent, watermarkBitmap, restrictionLayer, tileWidth, tileHeight);
                geoImage.Dispose();
            }
        }

        public void DrawOnBitmap(IEnumerable<Layer> layersTmp, GeographyUnit mapUnit, RectangleShape extent, GeoImage geoImage)
        {
            GeoCanvas canvas = new SkiaGeoCanvas();
            canvas.BeginDrawing(geoImage, extent, mapUnit);
            Collection<Layer> layers = LayerProvider.GetLayersToCache();
            foreach (Layer layer in layers)
            {
                if (!layer.IsOpen)
                {
                    layer.Open();
                }
                layer.Draw(canvas, new Collection<SimpleCandidate>());
            }
            canvas.EndDrawing();
        }

        private void SaveBitmapToTiles(FileRasterTileCache tileCache, TileMatrix tileMatrix, GeoImage geoImage, RectangleShape bitmapExtent, RectangleShape cachingExtent, Bitmap watermarkBitmap, ShapeFileFeatureLayer restrictionLayer, int tileWidth, int tileHeight)
        {
            MultipolygonShape resultShapes = ((AreaBaseShape)cachingExtent).GetIntersection(bitmapExtent);

            if (resultShapes != null)
            {
                RectangleShape cachingExtentInBitmapExtent = resultShapes.Polygons[0].GetBoundingBox();
                Collection<MatrixCell> cells = tileMatrix.GetIntersectingCells(cachingExtentInBitmapExtent);
                ZoomLevelSet zoomLevelSet = new ZoomLevelSet((int)(tileMatrix.TileWidth), GetBoundingboxFromMapUnit(), MapUnit);

                foreach (MatrixCell cell in cells)
                {
                    if (restrictionLayer == null || restrictionLayer.QueryTools.GetFeaturesIntersecting(cell.BoundingBox, ReturningColumnsType.NoColumns).Count > 0)
                    {
                        MemoryStream cellBitmap = GetCellFromBitmap(geoImage, bitmapExtent, cell.BoundingBox, tileWidth, tileHeight);
                        
                        var zoom = MapUtil.GetSnappedZoomLevelIndex(tileMatrix.Scale, zoomLevelSet);

                        RasterTile savingBitmapTile = new RasterTile(new GeoImage(cellBitmap), zoom, cell.Column, cell.Row);

                        if (watermarkBitmap != null && savingBitmapTile != null)
                        {
                            using (Graphics graphics = Graphics.FromImage((Image)savingBitmapTile.Bitmap.NativeImage))
                            {
                                graphics.DrawImageUnscaled(watermarkBitmap, 0, 0);
                            }
                        }

                        tileCache.SaveTile(savingBitmapTile);
                        cellBitmap.Dispose();
                        savingBitmapTile.Dispose();

                        lock (this)
                        {
                            currentTileIndex++;
                            int percentage = (int)(currentTileIndex * 100 / totalTilesCount);
                            if (percentage > 100)
                            {
                                percentage = 100;
                            }
                            OnProgressChanged(new CacheGeneratorProgressChangedEventArgs(percentage, null, currentTileIndex, totalTilesCount));
                        }
                    }
                }
            }
        }

        public MemoryStream GetCellFromBitmap(GeoImage sourceBitmap, RectangleShape sourceExtent, RectangleShape cellExtent, int tileWidth, int tileHeight)
        {
            Bitmap cellBitmap = new Bitmap(tileWidth, tileHeight);
            MemoryStream memoryStream = new MemoryStream();

            if (sourceBitmap != null)
            {
                Graphics graphics = null;
                
                try
                {
                    ScreenPointF upperLeftPoint = MapUtil.ToScreenCoordinate(sourceExtent, cellExtent.UpperLeftPoint, Convert.ToSingle(sourceBitmap.Width), Convert.ToSingle(sourceBitmap.Height));
                    graphics = Graphics.FromImage(cellBitmap);
                    graphics.DrawImageUnscaled(new Bitmap(sourceBitmap.GetImageStream(GeoImageFormat.Png)), -(int)Math.Round(upperLeftPoint.X), -(int)Math.Round(upperLeftPoint.Y));
                    cellBitmap.Save(memoryStream, ImageFormat.Png);
                    memoryStream.Seek(0, SeekOrigin.Begin);
                }
                finally
                {
                    if (graphics != null) { graphics.Dispose(); }
                    if (cellBitmap != null) { cellBitmap.Dispose(); }
                }
            }

            return memoryStream;
        }

        public long GetTilesCount()
        {
            long tilesCount = 0;
            Collection<long> cells = GetTilesCountsForScales();
            foreach (long cellCount in cells)
            {
                tilesCount += cellCount;
            }
            return tilesCount;
        }

        public Collection<long> GetTilesCountsForScales()
        {
            if (!string.IsNullOrEmpty(restrictShapeFilePathName))
            {
                restrictionLayer = new ShapeFileFeatureLayer(restrictShapeFilePathName, FileAccess.Read);
                restrictionLayer.Open();
            }

            RectangleShape boundingbox = GetBoundingboxFromMapUnit();

            Collection<long> cells = new Collection<long>();
            foreach (double scale in scales)
            {
                TileMatrix tileMatrix = new TileMatrix(scale, tileWidth, tileHeight, boundingbox, mapUnit);
                RowColumnRange rowColumnRange = tileMatrix.GetIntersectingRowColumnRange(cachingExtent);
                long deltaColumn = rowColumnRange.MaxColumnIndex - rowColumnRange.MinColumnIndex + 1;
                long deltaRow = rowColumnRange.MaxRowIndex - rowColumnRange.MinRowIndex + 1;

                long cellsCountUnderScale = deltaColumn * deltaRow;
                if (restrictionLayer != null)
                {
                    Collection<Feature> features = restrictionLayer.QueryTools.GetFeaturesInsideBoundingBox(cachingExtent, ReturningColumnsType.NoColumns);
                    double totalArea = cachingExtent.Width * cachingExtent.Height;
                    double area = 0;
                    foreach (Feature feature in features)
                    {
                        // Use the same unit here (Meter and SquarMeter) to just make the calculation easy.
                        area += ((AreaBaseShape)feature.GetShape()).GetIntersection(cachingExtent).GetArea(GeographyUnit.Meter, AreaUnit.SquareMeters);
                    }
                    cellsCountUnderScale = (long)Math.Ceiling(cellsCountUnderScale * area / totalArea);
                }
                cells.Add(cellsCountUnderScale);
            }
            return cells;
        }

        public void Close()
        {
            if (workingThreads != null)
            {
                foreach (Thread workingThread in workingThreads)
                {
                    if (workingThread != null && workingThread.IsAlive)
                    {
                        workingThread.Abort();
                    }

                }
            }
            if (layersToCache != null)
            {
                foreach (Layer layer in layersToCache)
                {
                    if (layer.IsOpen)
                    {
                        layer.Close();
                    }
                }
            }

            if (restrictionLayer != null && restrictionLayer.IsOpen)
            {
                restrictionLayer.Close();
            }
        }

        protected virtual void OnProgressChanged(CacheGeneratorProgressChangedEventArgs e)
        {
            EventHandler<CacheGeneratorProgressChangedEventArgs> handler = ProgressChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void OnGenerationCompleted(EventArgs e)
        {
            EventHandler<EventArgs> handler = GenerationCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private Bitmap GetWatermarkBitmap()
        {
            if (watermarkBitmap == null)
            {
                return null;
            }

            Bitmap clonedWatermarkBitmap = null;
            lock (watermarkBitmap)
            {
                clonedWatermarkBitmap = (Bitmap)watermarkBitmap.Clone();
            }
            return clonedWatermarkBitmap;
        }

        private ShapeFileFeatureLayer GetRestrictionLayer()
        {
            if (restrictionLayer == null)
            {
                return null;
            }

            ShapeFileFeatureLayer clonedRestrictionLayer = new ShapeFileFeatureLayer(restrictionLayer.ShapePathFilename, FileAccess.Read);
            clonedRestrictionLayer.Open();

            return clonedRestrictionLayer;
        }
    }
}